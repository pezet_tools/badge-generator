from src.badge_generator.badge_generator import Badge
from unittest import TestCase
from pathlib import Path
import platform


class GenTestCase(TestCase):

    def test_gen_ok(self):
        template = Path(Path(__file__).parent.parent, 'src/badge_generator/templates/template1.svg')
        input_file = Path(Path(__file__).parent, platform.system().lower(), "input1.svg")

        badge = Badge(
            label="ORDER",
            value="100%",
            status="OK",
            template_path=template
        )
        badge.create()

        with open(input_file, 'r', encoding="utf-8") as f:
            input1 = f.read().replace(" ", "").replace("\n", "")

        self.assertEqual(input1.replace(" ", ""), badge.content.replace(" ", ""))
