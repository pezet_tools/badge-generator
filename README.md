# Badge generator
Tool for generating project badges from templates

## Getting started

### Installation

Use [uv - package and project manager](https://docs.astral.sh/uv/guides/) for the installation process.

```bash
# create environment
uv venv

# activate environment
. ./.venv/Scripts/activate

# sync the project's dependencies with the environment
uv sync
```

### Parameters
| Parameter               | Description                                                                                                                                                                          |
|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| -l, --label             | Main (left) text of the badge                                                                                                                                                        |
| -v, --value             | Status / progress (right) text of the badge                                                                                                                                          |
| -s, --status            | Status (color) of the badge. Available values are:<br><span style="color: #66b864">OK</span> -> ("#66b864")<br><span style="color: #e05d44">ERROR</span> -> ("#e05d44")<br><span style="color: #ca8c00">WARNING</span> -> ("#ca8c00")<br><span style="color: #3076af">INFO</span> -> ("#3076af") |
| -o, ---output [optional | Name of the output file. If not provided, svg contents will be printed out                                                                                                           |

### Usage

It is possible to use this tool as a standalone python module or by importing it to your own script.

#### Standalone
```bash
python -m badge_generator --label ORDER --value 100% --status OK
python -m badge_generator --label ORDER --value 100% --status OK --output file.svg
```

#### Imported

```text
# requests / pyproject.toml
badge-generator=="0.2.0"
```

```python
from badge_generator import Badge
badge = Badge(
        template_path=template,
        label=label,
        value=value,
        status=status,
        font=font,
        font_size=font_size,
    )
badge.create()
return badge.content
```
