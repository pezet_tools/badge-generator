import argparse
import math
import platform
from enum import Enum
from pathlib import Path
from typing import Final
from PIL import ImageFont


class Color(Enum):
    """
    Badge colors for different statuses
    """

    OK = "#66b864"
    ERROR = "#e05d44"
    WARNING = "#ca8c00"
    INFO = "#3076af"


class ColorNotFoundException(Exception):
    """
    Raised when user inputs a color name not defined in configuration
    """

    pass


def status_to_color(text: str) -> str:
    """
    Method mapping given status to badge color
    """
    try:
        return Color[text].value
    except KeyError as e:
        raise ColorNotFoundException from e


class Font(Enum):
    """
    Badge Font configuration
    """

    FONT = "DejaVuSans,Verdana,Geneva,sans-serif"
    SIZE = 11


def get_font(indx: int = 0) -> str:
    """
    Returns next font value from configured font family, delimited by comma.
    """
    fonts = str(Font.FONT.value).split(",")
    if platform.system() == "Windows":
        fonts = [f"{font.lower()}.ttf" for font in fonts]
    elif platform.system() == "Linux":
        fonts = [f"/usr/share/fonts/truetype/{font.lower()}/{font}.ttf" for font in fonts]
    return fonts[indx]


class Badge:
    """
    Badge object
    """

    PADDING: Final[int] = 6

    def __init__(
        self,
        label: str,
        value: str,
        status: str,
        template_path: str | Path = Path(Path(__file__).parent, Path('templates/template1.svg')),
        font: str = Font.FONT.value,
        font_size: int = Font.SIZE.value,
    ):
        """
        :param template_path: Filepath to svg template
        :param label: Badge text (left-text)
        :param value: Badge value (right text)
        :param status: Badge status [OK, ERROR, WARNING, INFO] - each value is mapped on a color
        :param font: Font of a badge text
        :param font_size: Size of a badge text
        """
        self.template_path = Path(template_path)
        self.parameters = {
            "label": label,
            "value": value,
            "status": status,
            "font": font,
            "font_size": font_size,
            "label_pos": 6,
        }
        self.parameters.update(
            {
                "bg_label_width": self.PADDING - 1 + self.calculate_length(self.parameters["label"]) + self.PADDING,
                "bg_value_width": self.PADDING + self.calculate_length(self.parameters["value"]) + self.PADDING,
            }
        )
        self.parameters.update(
            {
                "bg_width": self.parameters["bg_label_width"] + self.parameters["bg_value_width"],
                "value_pos": self.parameters["bg_label_width"] + self.PADDING - 1,
            }
        )
        try:
            self.parameters.update({"bg_value_color": status_to_color(self.parameters["status"])})
        except ColorNotFoundException:
            print(f"Color mapping for {self.parameters['status']} does not exist!")
            exit(1)

        self.content: str = ""

    @staticmethod
    def calculate_length(text: str, size: int = None) -> float:
        """
        :param text: Text which length should be calculated
         :param size: Text size
         :return: Text length in pixels
        """
        indx = 0
        while True:
            try:
                font = ImageFont.truetype(
                    get_font(indx), size if size else Font.SIZE.value
                )
                return math.ceil(font.getlength(text))
            except OSError:
                indx += 1

    def generate_from_template(self, template: str):
        """
        Replaces template values
        """
        badge_content: str = template
        for key, value in self.parameters.items():
            badge_content = badge_content.replace("${" + key + "}", str(value))
        self.content = badge_content

    def create(self):
        """
        Main function for badge creation
        """
        if self.template_path.exists():
            with open(self.template_path, "r", encoding="utf-8") as f:
                badge_template = f.read()
                self.generate_from_template(badge_template)
        else:
            print(f"ERROR: [{self.template_path.as_posix()}] template file does not exist!")
            exit(1)


def save_badge(badge_content: str, output_filepath: str):
    """
    Saves generated badge to a file
    """
    output_filepath = (
        output_filepath if isinstance(output_filepath, Path) else Path(output_filepath)
    )
    if not output_filepath.is_absolute():
        output_filepath = Path.cwd() / output_filepath
    output_filepath.parent.mkdir(parents=True, exist_ok=True)
    with open(output_filepath, "w", encoding="utf-8") as o:
        o.write(badge_content)


def parse_args():
    """
    Parses application arguments
    """
    parser = argparse.ArgumentParser(
        prog="Badge Generator",
        description="Generates project badge images (svg) based on templates",
    )

    parser.add_argument("-l", "--label",
                        required=True, 
                        help="Badge label text")
    parser.add_argument("-v", "--value", 
                        required=True, 
                        help="Badge value text")
    parser.add_argument("-s", "--status", 
                        required=True, 
                        help="Badge status")
    parser.add_argument("-t",
                        "--template",
                        default=Path(Path(__file__).parent, Path('templates/template1.svg')),
                        help="Badge file template")
    parser.add_argument("-fs", "--font-size", 
                        default=Font.SIZE.value, 
                        help="Font size")
    parser.add_argument("-f", "--font", 
                        default=Font.FONT.value, 
                        help="Font")
    parser.add_argument("-o", "--output", 
                        help="Output pathfile for svg file")

    return parser.parse_args()


def main():
    args = parse_args()

    badge = Badge(
        label=args.label,
        value=args.value,
        status=args.status,
        template_path=args.template,
        font=args.font,
        font_size=args.font_size,
    )
    badge.create()

    if args.output:
        save_badge(badge.content, args.output)
    else:
        print(badge.content)


if __name__ == "__main__":
    main()
